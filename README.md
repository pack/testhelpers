# testhelpers

This is a suite of `testhelpers` to be used when running tests with containers.

It has primitives around handling test cases as well as handlers for waiting on ports.

**Example Mysql Test**:

```go

var dockerTestTags = []string{"ci_testhelper"} // Note this is used for test cleanup

var testImageTag = "mariadb:10.3"
var testDockerEnv = []string{
  "MYSQL_ROOT_PASSWORD=RickC137",
  "MYSQL_DATABASE=testdb",
}

func TestImageStart(t *testing.T) {
  cont, err := NewDockerContainer(t,
    testImageTag,
    testDockerEnv,
    []string{},
    dockerTestTags,
  )

  if err != nil {
    t.Fatal(err)
  }

  err = cont.Retry(func() error {
    portMap, err := cont.PortMapFor(3306)
    if err != nil {
      return err
    }

    conn, err := net.Dial("tcp", portMap.ConnectionURI())
    if err != nil {
      return err
    }
    conn.Close()
    return err
  })

  if err != nil {
    t.Fatal(err)
  }
}
```
