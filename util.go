package testhelpers

import (
	"crypto/rand"
	"crypto/sha1"
)

func GenerateRandomBytes(length int) ([]byte, error) {
	b := make([]byte, length)
	_, err := rand.Read(b)
	return b, err
}

func GenerateRandomSha1(length int) ([sha1.Size]byte, error) {
	b, err := GenerateRandomBytes(length)
	if err != nil {
		return [sha1.Size]byte{}, err
	}
	return sha1.Sum(b), err
}
