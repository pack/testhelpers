TEST?=$(shell glide novendor)

default: test

test:
	go test $(TEST) $(TESTARGS) -timeout=30s -parallel=4

# testv runs the unit tests verbosely
testv: TESTARGS=-v
testv: test

watch:
	$(info Watching go files...)
	@fswatch -r -0 -e ./vendor *.go | xargs -0 -n 1 -I {} bash -c 'make testv; echo'
