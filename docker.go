package testhelpers

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/cenkalti/backoff"
	dockertypes "github.com/docker/docker/api/types"
	dockercontainer "github.com/docker/docker/api/types/container"
	dockernetwork "github.com/docker/docker/api/types/network"
	dockerclient "github.com/docker/docker/client"
)

// NewDockerContainer returns a new docker container and starts it.
func NewDockerContainer(t testing.TB, image string, env []string, cmd []string, tags []string) (*DockerContainer, error) {
	c, err := dockerclient.NewEnvClient()
	if err != nil {
		return nil, err
	}

	if cmd == nil {
		cmd = make([]string, 0)
	}

	contr := &DockerContainer{
		t:         t,
		client:    c,
		ImageName: image,
		ENV:       env,
		Cmd:       cmd,
		PortMaps:  make(map[uint]PortMap),
	}

	if err := contr.PullImage(); err != nil {
		return nil, err
	}

	if err := contr.Start(tags...); err != nil {
		return nil, err
	}

	return contr, nil
}

// PortMap is a helper struct for referencing host to container NAT configurations.
type PortMap struct {
	ContainerPort uint   `json:"container_port"`
	HostIP        string `json:"host_ip"`
	HostPort      uint   `json:"host_port"`
}

// ConnectionURI returns the connection URI for connecting to the container port NAT.
func (p *PortMap) ConnectionURI() string {
	return fmt.Sprintf("%s:%d", p.ExternalHost(), p.HostPort)
}

// ExternalHost is the binding IP for connections.
func (p *PortMap) ExternalHost() string {
	if p.HostIP == "0.0.0.0" {
		return p.dockerIP()
	}
	return p.HostIP
}

func (p *PortMap) dockerIP() string {
	host := os.Getenv("DOCKER_HOST")
	if strings.HasPrefix(host, "unix://") {
		return "127.0.0.1"
	}

	parts := strings.Split(strings.TrimPrefix(host, "tcp://"), ":")
	return parts[0]
}

// DockerContainer implements Instance interface
type DockerContainer struct {
	t                  testing.TB
	client             *dockerclient.Client
	ImageName          string
	ENV                []string
	Cmd                []string
	ContainerID        string
	ContainerName      string
	ContainerJSON      dockertypes.ContainerJSON
	containerInspected bool
	KeepForDebugging   bool
	MaxWait            time.Duration
	PortMaps           map[uint]PortMap
	DefaultPortMap     PortMap
}

// PullImage retrieves the provided image name.
func (d *DockerContainer) PullImage() error {
	d.t.Logf("Docker: Pull image %v", d.ImageName)
	r, err := d.client.ImagePull(context.Background(), d.ImageName, dockertypes.ImagePullOptions{})
	if err != nil {
		return err
	}
	defer r.Close()

	// read output and log relevant lines
	bf := bufio.NewScanner(r)
	for bf.Scan() {
		var resp dockerImagePullOutput
		if err := json.Unmarshal(bf.Bytes(), &resp); err != nil {
			return err
		}
		if strings.HasPrefix(resp.Status, "Status: ") {
			d.t.Logf("Docker: %v", resp.Status)
		}
	}
	return bf.Err()
}

// Start creates a new docker container with the pack_test prefix and tags
// as well as it's own unique name.
func (d *DockerContainer) Start(tags ...string) error {
	shaSum, err := GenerateRandomSha1(16)
	if err != nil {
		return err
	}

	containerName := fmt.Sprintf("pack_test_%x", shaSum)
	labelMap := map[string]string{"pack_test": "true"}
	for _, tag := range tags {
		labelMap[tag] = "true"
	}

	// create container first
	resp, err := d.client.ContainerCreate(context.Background(),
		&dockercontainer.Config{
			Image:  d.ImageName,
			Labels: labelMap,
			Env:    d.ENV,
			Cmd:    d.Cmd,
		},
		&dockercontainer.HostConfig{
			PublishAllPorts: true,
		},
		&dockernetwork.NetworkingConfig{},
		containerName)
	if err != nil {
		return err
	}

	d.ContainerID = resp.ID
	d.ContainerName = containerName

	// then start it
	if err := d.client.ContainerStart(context.Background(), resp.ID, dockertypes.ContainerStartOptions{}); err != nil {
		return err
	}

	d.updatePortMap()

	d.t.Logf("Docker: Started container %v (%v) for image %v listening at %v:%v", resp.ID[0:12], containerName, d.ImageName, d.DefaultPortMap.ExternalHost(), d.DefaultPortMap.HostPort)
	for _, v := range resp.Warnings {
		d.t.Logf("Docker: Warning: %v", v)
	}
	return nil
}

// Remove the container from docker.
func (d *DockerContainer) Remove() error {
	if d.KeepForDebugging {
		d.t.Logf("Docker: Keeping container for debugging: %s", d.ContainerName)
		return nil
	}

	if len(d.ContainerID) == 0 {
		return fmt.Errorf("missing containerID")
	}
	if err := d.client.ContainerRemove(context.Background(), d.ContainerID,
		dockertypes.ContainerRemoveOptions{
			Force: true,
		}); err != nil {
		d.t.Log(err)
		return err
	}
	d.t.Logf("Docker: Removed %v", d.ContainerName)
	return nil
}

// Inspect retrieves the container JSON information.
func (d *DockerContainer) Inspect() error {
	if len(d.ContainerID) == 0 {
		return fmt.Errorf("missing containerID")
	}
	resp, err := d.client.ContainerInspect(context.Background(), d.ContainerID)
	if err != nil {
		return err
	}

	d.ContainerJSON = resp
	d.containerInspected = true
	return nil
}

// Logs returns the io.ReadCloser handle for the container logs.
func (d *DockerContainer) Logs() (io.ReadCloser, error) {
	if len(d.ContainerID) == 0 {
		return nil, fmt.Errorf("missing containerID")
	}

	return d.client.ContainerLogs(context.Background(), d.ContainerID, dockertypes.ContainerLogsOptions{
		ShowStdout: true,
		ShowStderr: true,
	})
}

func (d *DockerContainer) updatePortMap() (err error) {
	var foundFirst bool
	var pMap PortMap

	if !d.containerInspected {
		if err := d.Inspect(); err != nil {
			return err
		}
	}

	for port, bindings := range d.ContainerJSON.NetworkSettings.Ports {
		for _, binding := range bindings {
			hostPortUint, err := strconv.ParseUint(binding.HostPort, 10, 64)
			if err != nil {
				return err
			}

			containerPortUint := uint(port.Int())
			pMap = PortMap{
				ContainerPort: containerPortUint,
				HostIP:        binding.HostIP,
				HostPort:      uint(hostPortUint),
			}

			if !foundFirst {
				d.DefaultPortMap = pMap
				foundFirst = true
			}

			d.PortMaps[containerPortUint] = pMap
		}
	}
	return nil
}

// PortMapFor will take a given connection port and find the associated PortMap.
func (d *DockerContainer) PortMapFor(cPort uint) (PortMap, error) {
	pMap := d.PortMaps[cPort]
	if pMap.HostIP == "" {
		return pMap, fmt.Errorf("specified port not bound")
	}

	return pMap, nil
}

func (d *DockerContainer) NetworkSettings() dockertypes.NetworkSettings {
	netSettings := d.ContainerJSON.NetworkSettings
	return *netSettings
}

type dockerImagePullOutput struct {
	Status          string `json:"status"`
	ProgressDetails struct {
		Current int `json:"current"`
		Total   int `json:"total"`
	} `json:"progress_details"`
	ID       string `json:"id"`
	Progress string `json:"progress"`
}

// CopyFrom will read a file from a container and return a io.ReadCloser to handle your own reader.
func (d *DockerContainer) CopyFrom(source string, destination io.ReadCloser) (io.ReadCloser, dockertypes.ContainerPathStat, error) {
	return d.client.CopyFromContainer(context.Background(), d.ContainerID, source)
}

// CopyFromToFile will read a file from a container and write it to a local file destination.
func (d *DockerContainer) CopyFromToFile(source, destination string) error {
	in, _, err := d.client.CopyFromContainer(context.Background(), d.ContainerID, source)
	if err != nil {
		return err
	}
	defer in.Close()

	out, err := os.Create(destination)
	if err != nil {
		return err
	}

	_, err = io.Copy(out, in)
	return err
}

// CopyTo will read from a local io.ReadCloser and copy it to a container destination.
func (d *DockerContainer) CopyTo(in io.ReadCloser, targetContainer, destination string) error {
	return d.client.CopyToContainer(context.Background(), targetContainer, destination, in,
		dockertypes.CopyToContainerOptions{AllowOverwriteDirWithFile: false})
}

// CopyFileTo will read a file from a local file and copy it to a container destination.
func (d *DockerContainer) CopyFileTo(source, targetContainer, destination string) error {
	in, err := os.Open(source)
	if err != nil {
		return err
	}
	defer in.Close()

	return d.client.CopyToContainer(context.Background(), targetContainer, destination, in,
		dockertypes.CopyToContainerOptions{AllowOverwriteDirWithFile: false})
}

// Retry is an exponential backoff helper for running docker commands.
func (d *DockerContainer) Retry(op func() error) error {
	bo := backoff.NewExponentialBackOff()
	bo.MaxInterval = time.Second * 2
	bo.MaxElapsedTime = d.MaxWait
	return backoff.Retry(op, bo)
}
