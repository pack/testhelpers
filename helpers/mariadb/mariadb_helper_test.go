package mariadbth

import (
	"testing"

	"bitbucket.org/pack/sqlutils"
)

func TestEnvOverrides(t *testing.T) {
	t.Parallel()

	expected := []string{
		"MYSQL_DATABASE=my_override_db",
		"MYSQL_ROOT_PASSWORD=root_override_password",
	}

	th := &MysqlTestHelper{
		Options: &sqlutils.MysqlOptions{
			Database: "my_override_db",
			Password: "root_override_password",
		},
	}

	for i, env := range th.DockerEnv() {
		if env != expected[i] {
			t.Errorf("EnvOverrides do not match; received: `%s` expected: `%s`", env, expected[i])
		}
	}
}

func TestMysqlConnection(t *testing.T) {
	t.Parallel()

	th := &MysqlTestHelper{}
	_, err := th.Connect(t)
	defer th.Cleanup()
	if err != nil {
		t.Error(err)
	}
}
