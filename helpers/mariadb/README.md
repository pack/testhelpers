# testhelpers-mariadb

This is a [testhelper](https://bitbucket.org/pack/testhelpers) for running [mariadb](https://mariadb.org/about/) containers to mock `mysql` for local development.

## Usage

```go
// in some _test.go file
import (
  "testing"

  "bitbucket.org/pack/testhelpers/helpers/mariadb"
)

func TestMyApp(t *testing.T) {
  th := &mariadbth.MysqlTestHelper{}
  _, err := th.Connect(t)

  defer th.Cleanup()
  if err != nil {
    t.Error(err)
  }
}
```
