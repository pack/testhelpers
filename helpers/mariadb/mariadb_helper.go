package mariadbth

import (
	"database/sql"
	"fmt"
	"testing"

	"bitbucket.org/pack/sqlutils"
	"bitbucket.org/pack/testhelpers"
	_ "github.com/go-sql-driver/mysql" // Required for sql import
)

// Hydra testing variables
var (
	DefaultMysqlImage = "mariadb"
	DefaultMysqlTag   = "10.3"

	DefaultMysqlDatabase = "testdb"
	DefaultMysqlUsername = "root"
	DefaultMysqlPassword = "helpersecret"

	mysqlTestTags = []string{"mariadb_testhelper"} // Note this is used for test cleanup
)

// MysqlTestHelper is a helper for testing with mysql.
type MysqlTestHelper struct {
	// Docker configurations
	Image string
	Tag   string

	// mysql client credentials
	Options *sqlutils.MysqlOptions

	// mysql client
	Container *testhelpers.DockerContainer
	DB        *sql.DB
}

// DockerEnv returns the environment structure for a mysql container.
func (th *MysqlTestHelper) DockerEnv() []string {
	return []string{
		fmt.Sprintf("MYSQL_DATABASE=%s", th.Options.Database),
		fmt.Sprintf("MYSQL_ROOT_PASSWORD=%s", th.Options.Password),
	}
}

// SetupDefaults will configure the default variables for a new mysql docker container.
func (th *MysqlTestHelper) SetupDefaults() error {
	if th.Image == "" {
		th.Image = DefaultMysqlImage
	}

	if th.Tag == "" {
		th.Tag = DefaultMysqlTag
	}

	if th.Options == nil {
		th.Options = &sqlutils.MysqlOptions{}
	}

	if th.Options.Database == "" {
		th.Options.Database = DefaultMysqlDatabase
	}

	if th.Options.Username == "" {
		th.Options.Username = DefaultMysqlUsername
	}

	if th.Options.Password == "" {
		th.Options.Password = DefaultMysqlPassword
	}
	return nil
}

// Connect will attempt (with retries) to connect to the docker mysql test client.
func (th *MysqlTestHelper) Connect(t *testing.T) (db *sql.DB, err error) {
	err = th.SetupDefaults()
	if err != nil {
		return nil, err
	}

	cont, err := testhelpers.NewDockerContainer(t,
		fmt.Sprintf("%s:%s", th.Image, th.Tag),
		th.DockerEnv(),
		[]string{},
		mysqlTestTags,
	)
	if err != nil {
		return &sql.DB{}, err
	}

	err = cont.Retry(func() error {
		portMap, err := cont.PortMapFor(3306)
		if err != nil {
			return err
		}

		th.Options.Host = portMap.ExternalHost()
		th.Options.Port = int(portMap.HostPort)
		db, err = sql.Open("mysql", th.Options.ConnectionString())
		th.DB = db
		if err != nil {
			return err
		}

		th.DB.SetMaxIdleConns(0)
		th.DB.SetMaxIdleConns(30)

		err = db.Ping()
		return err
	})
	return db, nil
}

// Cleanup will remove the container if it exists.
func (th *MysqlTestHelper) Cleanup() {
	if th.Container != nil {
		th.Container.Remove()
	}
}
