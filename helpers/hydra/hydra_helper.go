package hydrath

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"testing"
	"time"

	"bitbucket.org/pack/testhelpers"
	"github.com/ory/hydra/sdk"
)

// Hydra testing variables
var (
	DefaultHydraImage = "oryd/hydra"
	DefaultHydraTag   = "v0.10.0-alpha.7-http" // Note: For testing we just use the http port

	DefaultHydraClientID = "testhelper"
	DefaultHydraSecret   = "helpersecret"

	hydraTestTags = []string{"hydra_testhelper"} // Note: this is used for test cleanup
)

// HydraTestHelper is a helper for testing with ory hydra (oauth2 server).
type HydraTestHelper struct {
	// Docker configurations
	Image string
	Tag   string

	// Hydra init config
	SystemSecret string

	// Hydra client credentials
	ClientID     string
	ClientSecret string

	// Hydra consent app
	Issuer     string
	ConsentURL string

	// Hydra client
	Container *testhelpers.DockerContainer
	Client    *sdk.Client
}

func (th *HydraTestHelper) generateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

func (th *HydraTestHelper) generateRandomSha1() (string, error) {
	b, err := th.generateRandomBytes(24)
	if err != nil {
		return "", err
	}

	h := sha1.New()
	_, err = h.Write(b)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(h.Sum(nil)), nil
}

// DockerEnv returns the environment structure for a hydra container.
func (th *HydraTestHelper) DockerEnv() []string {
	return []string{
		fmt.Sprintf("SYSTEM_SECRET=%s", th.SystemSecret),
		"DATABASE_URL=memory",
		fmt.Sprintf("ISSUER=%s", th.Issuer),
		fmt.Sprintf("CONSENT_URL=%s", th.ConsentURL),
		fmt.Sprintf("FORCE_ROOT_CLIENT_CREDENTIALS=%s:%s", th.ClientID, th.ClientSecret),
	}
}

// SetupDefaults will configure the default variables for a new hydra docker container.
func (th *HydraTestHelper) SetupDefaults() error {
	if th.Image == "" {
		th.Image = DefaultHydraImage
	}

	if th.Tag == "" {
		th.Tag = DefaultHydraTag
	}

	if th.ClientID == "" {
		th.ClientID = DefaultHydraClientID
	}

	if th.ClientSecret == "" {
		th.ClientSecret = DefaultHydraSecret
	}

	sysSecret, err := th.generateRandomSha1()
	if err != nil {
		return err
	}
	th.SystemSecret = sysSecret
	return nil
}

func (th *HydraTestHelper) connectWithTimeout(host string, port uint, milliseconds time.Duration) (client *sdk.Client, err error) {
	ch := make(chan struct{})
	timeout := make(chan struct{})

	go func() {
		time.Sleep(milliseconds * time.Millisecond)
		timeout <- struct{}{}
	}()

	go func() {
		client, err = sdk.Connect(
			sdk.ClientID(th.ClientID),
			sdk.ClientSecret(th.ClientSecret),
			sdk.ClusterURL(
				fmt.Sprintf("http://%s:%d", host, port)),
		)
		ch <- struct{}{}
	}()

	select {
	case <-timeout:
		return nil, errors.New("Connection to Hydra timed out")
	case <-ch:
		break
	}

	return client, err
}

// Connect will attempt (with retries) to connect to the docker hydra test client.
func (th *HydraTestHelper) Connect(t *testing.T) (client *sdk.Client, err error) {
	err = th.SetupDefaults()
	if err != nil {
		return nil, err
	}

	cont, err := testhelpers.NewDockerContainer(t,
		fmt.Sprintf("%s:%s", th.Image, th.Tag),
		th.DockerEnv(),
		[]string{},
		hydraTestTags,
	)
	if err != nil {
		return nil, err
	}

	err = cont.Retry(func() error {
		portMap, err := cont.PortMapFor(4444)
		if err != nil {
			return err
		}

		client, err = th.connectWithTimeout(portMap.ExternalHost(), portMap.HostPort, 250)
		return err
	})

	return client, err
}

// Cleanup will remove the container if it exists.
func (th *HydraTestHelper) Cleanup() {
	if th.Container != nil {
		th.Container.Remove()
	}
}
