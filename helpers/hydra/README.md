# testhelpers-hydra

This is a [testhelper](https://bitbucket.org/pack/testhelpers) for running [hydra](https://github.com/ory/hydra) containers to mock `s3` for local development.

## Usage

```go
// in some _test.go file
import (
  "testing"

  "bitbucket.org/pack/testhelpers/helpers/minio"
)

func TestMyApp(t *testing.T) {
  th := &hydrath.HydraTestHelper{}
  _, err := th.Connect(t)

  defer th.Cleanup()
  if err != nil {
    t.Error(err)
  }
}
```
