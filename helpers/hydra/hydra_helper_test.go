package hydrath

import (
	"testing"
)

func TestHydraConnection(t *testing.T) {
	t.Parallel()

	th := &HydraTestHelper{
		Issuer:     "http://localhost:4444",
		ConsentURL: "http://localhost:4444/consent",
	}
	_, err := th.Connect(t)
	defer th.Cleanup()
	if err != nil {
		t.Error(err)
	}
}
