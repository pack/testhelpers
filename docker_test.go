package testhelpers

import (
	"context"
	"fmt"
	"net"
	"os"
	"testing"

	dockertypes "github.com/docker/docker/api/types"
	dockerfilters "github.com/docker/docker/api/types/filters"
	dockerclient "github.com/docker/docker/client"
)

var dockerTestTags = []string{"ci_testhelper"}

var testImageTag = "mariadb:10.3"
var testDockerEnv = []string{
	"MYSQL_ROOT_PASSWORD=RickC137",
	"MYSQL_DATABASE=testdb",
}

func TestImagePull(t *testing.T) {
	cont, err := NewDockerContainer(t,
		testImageTag,
		testDockerEnv,
		[]string{},
		dockerTestTags,
	)

	if err != nil {
		t.Fatal(err)
	}

	images, err := cont.client.ImageList(context.Background(), dockertypes.ImageListOptions{})
	if err != nil {
		t.Fatal(err)
	}

	foundImage := false
	for _, img := range images {
		for _, tag := range img.RepoTags {
			t.Log(tag)
			if tag == testImageTag {
				foundImage = true
				break
			}
		}
	}

	if !foundImage {
		t.Fatal("Unable to find the pulled docker image")
	}
}

func TestImageStart(t *testing.T) {
	cont, err := NewDockerContainer(t,
		testImageTag,
		testDockerEnv,
		[]string{},
		dockerTestTags,
	)

	if err != nil {
		t.Fatal(err)
	}

	err = cont.Retry(func() error {
		portMap, err := cont.PortMapFor(3306)
		if err != nil {
			return err
		}

		conn, err := net.Dial("tcp", portMap.ConnectionURI())
		if err != nil {
			return err
		}
		conn.Close()
		return err
	})

	if err != nil {
		t.Fatal(err)
	}
}

func TestMain(m *testing.M) {
	exitCode := m.Run()

	cleanupTestContainers()
	os.Exit(exitCode)
}

func cleanupTestContainers() {
	c, err := dockerclient.NewEnvClient()
	if err != nil {
		panic(err)
	}

	filters := dockerfilters.NewArgs()
	filters.Add("label", "pack_test")
	containers, err := c.ContainerList(context.Background(), dockertypes.ContainerListOptions{Filters: filters})
	if err != nil {
		fmt.Printf("Unable to list docker containers for cleanup: %v\n", err)
		return
	}

	for _, cnt := range containers {
		err := c.ContainerRemove(context.Background(), cnt.ID,
			dockertypes.ContainerRemoveOptions{
				RemoveVolumes: true,
				Force:         true,
			})

		if err != nil {
			fmt.Printf("Error removing container: %v\n", err)
		}
	}
}
